# 構成配列
word_ary = [
    'ド',
    'スコ',
]

# 正解配列
correct_ary = [
    'ド',
    'ド',
    'スコ',
    'スコ',
    'スコ',
    'スコ',
]

# 成功時に表示する文字列
success_str = 'ラブ注入☆'


# ppap関数
def ppap(word_ary, correct_ary, success_str)
    lists = []
    count = 0
    # 便利な関数
    while true do
        word = word_ary.sample
        lists << word
        # 正解の配列より短い場合は評価しない
        if lists.length > correct_ary.length then
            count = count + 1
            # 配列から最後の4つを取り出す
            candidate = lists.last(correct_ary.length)
            # 最後の4つがppap配列と一致する場合はsuccessと返す
            p candidate
            if candidate == correct_ary then 
                puts success_str
                puts '（' + count.to_s + '試行目）'
                break
            end
        end
    end
end

ppap(word_ary, correct_ary, success_str)
